FIND_PATH(LIBCVC4_INCLUDE_DIR NAMES cvc4/cvc4.h)
FIND_LIBRARY(LIBCVC4_LIBRARY NAMES cvc4)
FIND_LIBRARY(LIBCLN_LIBRARY NAMES cln)


INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LIBCVC4 DEFAULT_MSG LIBCVC4_LIBRARY LIBCLN_LIBRARY  LIBCVC4_INCLUDE_DIR)

IF(LIBCVC4_FOUND)
	SET(CVC4_LIBRARIES ${LIBCVC4_LIBRARY} ${LIBCLN_LIBRARY})
	SET(CVC4_INCLUDE_DIRS ${LIBCVC4_INCLUDE_DIR})
ELSE(LIBCVC4_FOUND)
		message(FATAL_ERROR "CVC4 not available")
ENDIF(LIBCVC4_FOUND)